package android.quider.pl.moneyrest;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by RENT on 2017-06-26.
 */

public class CurrenciesAdapter extends BaseAdapter {
    private Context appContext;
    private List<String> currencies;

    public CurrenciesAdapter(Context context, List<String> currencies) {
        this.appContext = context;
        this.currencies = currencies;
    }


    @Override
    public int getCount() {
        return currencies.size();
    }

    @Override
    public Object getItem(int position) {
        return currencies.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = View.inflate(appContext, R.layout.list_adapter, null);
        TextView listViewElement = (TextView) v.findViewById(R.id.listTextView);
        listViewElement.setText(currencies.get(position));
        return v;
    }
}